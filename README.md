# Browser testing React app with jasmine-browser-runner

Following the instructions [found in Jasmine docs](https://jasmine.github.io/tutorials/react_with_browser),
this repository is a template for browser testing of a React app.