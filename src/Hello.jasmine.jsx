import React from "react";
import {render, screen} from "@testing-library/react"
import userEvent from "@testing-library/user-event"
import JasmineDOM from "@testing-library/jasmine-dom";
import Hello from "./Hello";
beforeAll(() => { jasmine.getEnv().addMatchers(JasmineDOM); });
describe("Hello", function() {
    it(`displays "Hello"`, async () => {
        // ARRANGE
        render(<Hello />)
        // ACT
        await userEvent.click(screen.getByText("Hello"))
        const heading = await screen.findByRole("heading")
        // ASSERT
        expect(heading).toHaveTextContent("Hello")
      })
});